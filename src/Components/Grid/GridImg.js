import React, { Component } from 'react';
import '../../SCSS/Grid.scss';
import gridImg1 from '../../Images/gridImg1.png'
import gridImg2 from '../../Images/gridImg2.png'
import gridImg3 from '../../Images/gridImg3.png'
import gridImg4 from '../../Images/gridImg4.png'
import gridImg5 from '../../Images/gridImg5.png'
import gridImg6 from '../../Images/gridImg6.png'
import gridImg7 from '../../Images/gridImg7.png'
import gridImg8 from '../../Images/gridImg8.png'
import gridImg9 from '../../Images/gridImg9.png'
import gridImg10 from '../../Images/gridImg10.png'

class GridImg extends Component {
  render() {
    return (
      <div className='row'>
        <div className='column'>
            <div class="image-container">
                <img src={gridImg1} alt='sms' />
                <div class="after">+</div>
            </div>
            <div class="image-container">
                <img src={gridImg4} alt='sms' />
                <div class="after">+</div>
            </div>
            <div class="image-container">
                <img src={gridImg7} alt='sms' />
                <div class="after">+</div>
            </div>
        </div>
        <div className='column'>
            <div class="image-container">
                <img src={gridImg2} alt='sms' />
                <div class="after">+</div>
            </div>
            <div class="image-container">
                <img src={gridImg5} alt='sms' />
                <div class="after">+</div>
            </div>
            <div class="image-container">
                <img src={gridImg8} alt='sms' />
                <div class="after">+</div>
            </div>
            <div class="image-container">
                <img src={gridImg10} alt='sms' />
                <div class="after">+</div>
            </div>   
        </div>
        <div className='column third-column'>
            <div class="image-container">
                <img src={gridImg3} alt='sms' />
                <div class="after">+</div>
            </div>
            <div class="image-container">
                <img src={gridImg6} alt='sms' />
                <div class="after">+</div>
            </div>
            <div class="image-container">
                <img src={gridImg9} alt='sms' />
                <div class="after">+</div>
            </div>
        </div>         
      </div>
    );
  }
}

export default GridImg;
