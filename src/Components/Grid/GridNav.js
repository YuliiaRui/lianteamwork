import React, { Component } from 'react';
import '../../SCSS/Grid.scss';

class GridNav extends Component {
  render() {
    return (
      <div className='grid-nav-wrap'>
          <ul className='grid-nav'>
              <li>All</li>   
              <li className='grey-slash'></li>       
              <li>Logo</li> 
              <li className='grey-slash'></li>                
              <li>Mobile App</li>
              <li className='grey-slash'></li>       
              <li>WordPress</li>  
              <li className='grey-slash'></li>               
              <li>Web Design</li> 
              <li className='grey-slash'></li>                
              <li>UI/IX</li>
              <li className='grey-slash'></li>       
              <li>Branding</li>
          </ul>
      </div>
    );
  }
}

export default GridNav;