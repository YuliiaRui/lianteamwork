import React, { Component } from 'react';
import '../../SCSS/Grid.scss';

class GridBtn extends Component {
  render() {
    return (
      <div className='grid-btn'>
        <button>Load More</button>
      </div>
    );
  }
}

export default GridBtn;
