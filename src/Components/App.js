import React, { Component } from 'react';
import '../SCSS/App.scss';
import Grid from './Grid/Grid';
import Header from './Header'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <Grid/>
      </div>
    );
  }
}

export default App;
